#!/bin/sh

## SECURITY

# clear the logs
find /var/log -type f -delete

# clean apt
apt-get clean all

# delete ssh host keys and ask cloud-init to regenerate them at first boot
# ssh host keys need to be unique
rm -rf /etc/ssh/*key*
mkdir -p /var/lib/cloud/scripts/per-once
echo '#!/bin/bash
dpkg-reconfigure openssh-server' > /var/lib/cloud/scripts/per-once/sshkeys
chmod +x /var/lib/cloud/scripts/per-once/sshkeys

# delete the random seed
rm -f /var/lib/systemd/random-seed


## PERFORMANCE

# make kernel cmd line more friendly
sed -i s/'GRUB_CMDLINE_LINUX_DEFAULT="quiet"'/'GRUB_CMDLINE_LINUX_DEFAULT="vga=0x318 console=ttyS0,115200n8 console=hvc0 consoleblank=0 elevator=deadline biosdevname=0 net.ifnames=0"'/g /etc/default/grub
sed -i s/splash//g /etc/default/grub
sed -i s/\$vt_handoff//g /etc/default/grub
update-grub

# implementing some low level settings ad-labam, on CentOS tuned takes care of this - and more
echo kernel.sched_min_granularity_ns=10000000 >> /etc/sysctl.d/tuned.conf
echo kernel.sched_wakeup_granularity_ns=15000000 >> /etc/sysctl.d/tuned.conf
echo vm.dirty_ratio=40 >> /etc/sysctl.d/tuned.conf
echo vm.swappiness=1 >> /etc/sysctl.d/tuned.conf


# OTHER

# delete the udev rules for network devices
find /etc/udev/rules.d/ -name "*persistent*" -delete

# xs-tools
# workaround as debian installer can't mount a cdrom during install
wget -q https://[your URL]/xe-guest-utilities_7.10.0-0ubuntu1_amd64.deb
dpkg -i ./xe-guest-utilities_7.10.0-0ubuntu1_amd64.deb
rm -fv ./xe-guest-utilities_7.10.0-0ubuntu1_amd64.deb

# CLOUDSTACK

cat << "EOF" > /etc/cloud/cloud.cfg.d/99_cloudstack.cfg
datasource:
  CloudStack: {}
  None: {}
datasource_list: ["CloudStack"]
EOF

# passwd auth
cat << "EOF" >  /etc/cloud/cloud.cfg.d/80_pwauth.cfg
ssh_pwauth: 1
EOF

# make the cloud-init password module to run at every boot
sed -i s/"set-passwords"/"[set-passwords, always]"/g /etc/cloud/cloud.cfg


# by default cloud-init locks the password of the user, rendering cloudstack passwd feature useless, working around it below
cat << "EOF" > /etc/cloud/cloud.cfg.d/99_unlock.cfg
system_info:
   # This will affect which distro class gets used
   distro: debian
   # Default user name + that default users groups (if added/used)
   default_user:
     name: debian
     lock_passwd: False
EOF


cat << "EOF" > /etc/chrony/chrony.conf
pool ntp.usp.br iburst
pool pool.ntp.br iburst
keyfile /etc/chrony/chrony.keys
driftfile /var/lib/chrony/chrony.drift
logdir /var/log/chrony
maxupdateskew 100.0
rtcsync
makestep 1 3
EOF

rm -f /etc/update-motd.d/*

cat << "EOF" > /etc/update-motd.d/00-header
#!/bin/sh
[ -r /etc/lsb-release ] && . /etc/lsb-release

if [ -z "$DISTRIB_DESCRIPTION" ] && [ -x /usr/bin/lsb_release ]; then
	# Fall back to using the very slow lsb_release utility
	DISTRIB_DESCRIPTION=$(lsb_release -s -d)
fi

printf "Welcome to %s (%s %s %s)\n" "$DISTRIB_DESCRIPTION" "$(uname -o)" "$(uname -r)" "$(uname -m)"
EOF

cat << "EOF" > /etc/update-motd.d/10-sysinfo
#!/bin/bash
date=`date`
load=`cat /proc/loadavg | awk '{print $1}'`
root_usage=`df -h / | awk '/\// {print $(NF-1)}'`
memory_usage=`free -m | awk '/Mem:/ { total=$2; used=$3 } END { printf("%3.1f%%", used/total*100)}'`
 
swap_usage=`free -m | awk '/Swap/ { printf("%3.1f%%", $3/$2*100) }'`
users=`users | wc -w`
time=`uptime | grep -ohe 'up .*' | sed 's/,/\ hours/g' | awk '{ printf $2" "$3 }'`
processes=`ps aux | wc -l`
ip=`hostname -I | awk '{print $1}'`
 
echo "System information as of: $date"
echo
printf "System Load:\t%s\tIP Address:\t%s\n" $load $ip
printf "Memory Usage:\t%s\tSystem Uptime:\t%s\n" $memory_usage "$time"
printf "Usage On /:\t%s\tSwap Usage:\t%s\n" $root_usage $swap_usage
printf "Local Users:\t%s\tProcesses:\t%s\n" $users $processes
echo
EOF

chmod +x /etc/update-motd.d/*

# lock root account
usermod --lock root